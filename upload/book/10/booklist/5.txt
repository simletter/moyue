
    “你、你说什么……”

    “我说对啊！”苏君一脸的莫名其妙，“我弟弟和妹妹今年读高二，平常十点就睡了。我半夜两点起床的话，他们肯定不知道。”

    张玉彬深吸一口气：“这么说，你承认你没有不在场证明了？”

    “对。”

    又被这一个字堵回来，张玉彬强忍着心中的别扭，终于把目光放到了案访报告上，上面有探员对苏君家基本信息的介绍。

    “你有一个妹妹苏然，还有一个弟弟江安琦……哦，你弟弟原来不姓苏吗？你们的父母呢？”

    苏君随口答道：“我们小时候都在同一家孤儿院。后来孤儿院倒闭了，我们兄弟姐妹四个被安排到这里……”

    说到这里，他指了指门牌：“便民书店，晓得吧？一礼拜也不会有个客人来光顾，全靠政府发钱的那种。”

    张玉彬有些发愣，他知道城西区是平民区，却没想到还有这样一群人。

    “那你妹妹也姓苏……”

    “巧合而已。”

    苏君没有说谎，关于他们家的来历，这四周的街坊都很清楚，也因此会对他们有所照顾。

    本来还有个比他大四岁的姐姐，姓赵，书店最早也是她在经营。但在三个月前，大姐突然有事出了远门，书店才交到苏君手上。

    跟苏君这个高中勉强读完，根本考不上大学的学渣不同，他们大姐可一直是优等生。

    只是为了照顾三个弟妹，她连大学都是在安城本地读，否则完全可以考到省城去。

    确认过苏君家的背景之后，张玉彬也没有更多事好问了，很快和白婷萱告辞离开。

    “白师姐，你觉得这个苏君有没有问题？”张玉彬在走远之后，迫不及待地询问白婷萱，很想知道这位师姐的想法。

    白婷萱想了想道：“可能有。不过你最好祈祷这人不是凶手，否则你们有得查了。”

    ……

    城西区刑侦支队。

    张玉彬正与一群手下围坐在一张圆桌前，紧锣密鼓地查看着搜集来的线索，以及各地案访到的口供。

    刘松遇害案发生在五月十九日，被他们称为“5.19”凶杀案，已经制成档案汇总在警方的数据库内。

    “关于五一九凶杀案，现有的所有证据都在这里，大家说说想法吧。”张玉彬有些头疼地揉了揉太阳穴，敲着桌子道。

    旁边一名女警开口道：“张队，按照监控里的凶手身形比对，第四街道疑似犯人的总共只有三人，资料都在这里了。”

    张玉彬点点头，扫了一眼资料，苏君的名字赫然就在其中。

    他心中更加笃定了几分，但办案不是只凭怀疑就行，要有证据。况且这个案子里，还有一个最解释不通的地方……

    “找不到杀人动机。”方统，张玉彬的得力助手皱眉道，“我们查过第四街道的商户人家，尤其是这三个人，没有一个跟刘松有任何关联。”

    “刘松的社会关系呢？”张玉彬问道。

    方统当即回答：“也查过了。刘松早年有涉黑的情况，但四年前已经收手，很少有人知道他来到安城。而且他涉黑的程度不深，几个对头也没有能量追到安城来杀人。”

    刘松家没有财物丢失，他本人最近也不见跟谁有矛盾，完全找不到凶手的作案动机，这是让刑警队很头疼的一点。

    张玉彬眉头紧锁，突然看到白婷萱一言不发地坐在旁边，顿时心中一动。

    “今天先到这里吧，大家继续找线索。杀人动机，还有那三个嫌疑人，都可能是突破口！”

    “是！”

    ……

    刑警队的会议散场之后，张玉彬才坐到白婷萱对面，说道：“师姐，我觉得那个苏君就是凶手！”

    白婷萱轻笑看着他道：“为什么这么说？就凭凶手进了第四街道，就没再出来过？”

    张玉彬微微一愣：“这是一方面，主要是我感觉……”

    “凶手可能知道那里没有监控，找个角落躲到白天才出来，也可能直接跳河离开，办法多的是。”白婷萱沉声道，“作为一个刑警，你就是凭感觉办案的？”

    张副队长被她说得无言以对，片刻后才惭愧道：“对不起，师姐，是我着急了……不过我还是觉得苏君有问题，我一定会找到证据！”

    白婷萱叹了口气道：“我的老师，宗无腾教授写过一本书，叫做《无解的犯罪》，有空你可以去看看。”

    说到这里，她就站起身来向外走去，摆了摆手示意张玉彬不用跟过来。

    ……

    苏君依旧在自家书店里看着书。

    正如先前所说，这家便民书店其实很是冷清，大半个月也不见得有一个客人，有也是进来看看书，打发时间，买书的基本见不到。

    这也给了苏君安静看书的机会。他看的书非常杂，从历史人文到科技发展，从武学体系到政治格局，没有什么是他不看的。

    三百年的空档他必须补上，当然这其中也不是没有主次，最先需要补上的当然是武学。

    三百年时间过去，可以说随着科技发展，人们的生活已经大变样，但这并不代表武学就没有用了。

    相反现在的联邦也远远称不上无敌，北方有妖蛮肆虐，大洋彼岸还有着另一个人类政权——帝国。

    武学的发展也远远超出苏君的预料。

    武道七境的体系依旧被沿用，但当年各家各族独有的珍贵武学，如今却被联邦统一收纳，以半公开的形式提供给民众。

    所有在武道协会登记在册的武者，都可以免费挑选喜爱的武学，只是有着额度限制，同时也只能拿到对应自己段位的篇幅。

    但如果想要后面的内容，其实只要付钱就可以了，价钱谈不上多贵。

    “靠，还以为一周目学的武功能卖个好价钱……”苏君有些无语地嘟囔道。

    他当年也收集过不少秘藏，没想到在现代，全部都成了大路货。

    不过他也必须要承认，如果没有人敝帚自珍，所有人都把压箱底的武学拿出来交流，对武术的整体发展绝对是好事。

    在三百年前，他一个死生境就算一方大高手，而现代连第六境的高人都不在少数。

    “任重而道远啊……”苏君叹息着将这本《现代武林》放回书架，随即猛地身形一顿，转头看向门口。

    一道轻飘飘的影子一步踏入屋内，猛然一掌打向苏君的心口！
