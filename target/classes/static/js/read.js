//看书时 目录旁几个进行切换
function bookreve(inter) {
    inter.toggleClass('none');
}
function bookmu() {
    $('.read-left-one').toggleClass('none');
    $('.read-left-two').addClass('none');
}
function bookgeng() {
    $('.read-left-two').toggleClass('none');
    $('.read-left-one').addClass('none');
}
//改变文本
function readbook(aa,bb) {
    var read=$('.read-center');
    if(aa!=""){
        var bgc0=$('.bgc-0');
        for(var i=0;i<bgc0.length;i++){
            $(bgc0[i]).removeClass('bgc');
        }
        aa.addClass('bgc');
        var bc=aa.css("background-color");
        read.css({"background-color":bc})
        $('.read-left-two').css({"background-color":bc});
        $('.read-left-one').css({"background-color":bc});
        $('.read-left dl').css({"background-color":bc});
    }
    if(bb!=""){
        var ztx=$('.ztx');
        for(var i=0;i<ztx.length;i++){
            $(ztx[i]).removeClass('ztx-0');
        }
        bb.addClass('ztx-0');
        var zt=bb.css("font-family");
        read.css({"font-family":zt})
    }
}
//字体大小  页面宽度
function sizejx() {
    var long=$('#long');
    var longs=parseInt(long.text());
    longs=longs-2;
    long.html(longs);
    var read=$('.read-center');
    read.css({"font-size":longs});
}
function sizezd() {
    var long=$('#long');
    var longs=parseInt(long.text());
    longs=longs+2;
    long.html(longs);
    var read=$('.read-center');
    read.css({"font-size":longs});
}
function sizebk() {
    var long=$('.ye-long');
    var longs=parseInt(long.text());
    longs=longs+100;
    long.html(longs);
    var read=$('.read-center');
    read.css({"width":longs});
}
function sizebz() {
    var long=$('.ye-long');
    var longs=parseInt(long.text());
    longs=longs-100;
    long.html(longs);
    var read=$('.read-center');
    read.css({"width":longs});
}
//回到顶部
function huitop() {
    var timeEd = null;
    cancelAnimationFrame(timeEd);
    timeEd = requestAnimationFrame(function fn(){
        var oTop = document.body.scrollTop || document.documentElement.scrollTop;
        if(oTop > 0){
            scrollBy(0,-100);
            timeEd = requestAnimationFrame(fn);
        }else{
            cancelAnimationFrame(timeEd);
        }
    });
}
//订阅
var allbook=[];//订阅的章节集合
var num=0;//订阅总钱数
var fontnum=0;//订阅总字数
//订阅章节 全选
function selectAllVolume() {
// 获取要操作的复选框
    var list=$('.selectall');
//checked判断是否选中
    allbook.splice(0,allbook.length);
    for(var x=0;x<list.length;x++) {
        var lists=list[x];
        lists.checked=true;
    }
    allselect();
    selectco();
}
//订阅章节   取消
function SelectNOVolume(){
    // 获取要操作的复选框
    var list=$('.selectall');
    allbook.splice(0,allbook.length);
//checked判断是否选中
    for (var x = 0; x < list.length; x++) {
        var lists=list[x];
        lists.checked=false;
    }
    allselect();
    selectco();
}
//点击单独的章节
$('.selectall').on('click',function () {
    allselect();
    selectco();
});
function allselect(){
    var list=$('.selectall');
    num=0;
    fontnum=0;
    allbook.splice(0,allbook.length);
    for (var x = 0; x < list.length; x++) {
        var value1=list[x];
        // console.log($(idf).text());
        if(list[x].checked==true){
            var idf=$(value1).parent().next().next().next();
            var font=$(value1).parent().next().next();
            var nums=parseInt($(idf).text());
            var fonts=parseInt($(font).text());
            allbook.push($(list[x]).val());
            num=num+nums;
            fontnum=fontnum+fonts;
        }
    }
    $('#moyue').text(num);
    $('.selectnum').text(fontnum);
    $('#chex').text(allbook.length);
}
//订阅按钮的颜色
function selectco(){
    if($('#moyue').text() != 0){
        $('.yesdingyue').css({"background-color":"#ed4259","color":"#fff","cursor": "pointer"});
    }else {
        $('.yesdingyue').css({"background-color":"#a6a6a6","color":"#fff","cursor": "default"});
    }
}
//确认订阅
function yesselect() {
    $('.yesdingyue').css({"background-color":"#a6a6a6","color":"#fff","cursor": "default"});
    $.ajax({
        type: "GET",//数据发送的方式（post 或者 get）
        url: "/moyue/user/jundgeuser",//要发送的后台地址
        data: {
        },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
        dataType:"JSON",
        success: function (data) {//ajax请求成功后触发的方法
            if (data.code ==  0){
                $('.seleteshu').val(allbook);
                $.ajax({
                    type: "POST",//数据发送的方式（post 或者 get）
                    url: "/moyue/user/buychapter",//要发送的后台地址
                    data: {
                        chapters : $('.seleteshu').val(),
                    },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
                    dataType:"JSON",
                    success: function (data) {//ajax请求成功后触发的方法
                        if (data.code ==  0){
                            $('.yesselect').text(data.msg);
                            yesselectss();
                            setTimeout("noselectss()",2000);
                            SelectNOVolume();
                            location.reload();
                        }else{
                            var datas=data.msg;
                            var cc=[];
                            cc=datas.split(",");
                            $('.suffx').text(cc[0]);
                            $('.suffs').text(cc[1])
                            suff();
                        }
                    },
                    error: function (msg) {//ajax请求失败后触发的方法
                        alert("网络错误");//弹出错误信息
                    }
                });
            }else{
                alert('请先进行登录');
                upup();
            }
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("网络错误");//弹出错误信息
        }
    });


}
//最后确认订阅成功
function yesselectss() {
    $('.yesselect').addClass('block').removeClass('none');
}
function noselectss() {
    $('.yesselect').addClass('none').removeClass('block');
}

//打赏
function colorpop(table,spans) {

    var tablexx=$('.popup-wrip');
    for (var i=0;i<tablexx.length;i++){
        $(tablexx[i]).removeClass('popup-wrip-s');
    }
    table.addClass('popup-wrip-s');
    $('#currency').html(spans);
}
//打赏时用户是否登录
function ynuser() {
    $.ajax({
        type: "GET",//数据发送的方式（post 或者 get）
        url: "/moyue/user/jundgeuser",//要发送的后台地址
        data: {
        },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
        dataType:"JSON",
        success: function (data) {//ajax请求成功后触发的方法
           if (data.code ==  0){
               dashang();
               $.ajax({
                   type: "POST",//数据发送的方式（post 或者 get）
                   url: "/moyue/user/forwordReword",//要发送的后台地址
                   data: {
                   },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
                   dataType:"JSON",
                   success: function (data) {//ajax请求成功后触发的方法
                       if (data.code ==  0){
                           $('#account').text(data.data);
                       }else{}
                   },
                   error: function (msg) {//ajax请求失败后触发的方法
                       alert("网络错误");//弹出错误信息
                   }
               });
           }else{
               upup();
           }
           },
        error: function (msg) {//ajax请求失败后触发的方法
             alert("网络错误");//弹出错误信息
        }
 });
}
function dashang(){
    var ds = document.getElementsByClassName('reward')[0];
    var back = document.getElementById('back');
    back.style.display = "block";
    ds.style.display = "block";
}
function dsclose(){
    var ds = document.getElementsByClassName('reward')[0];
    var back= document.getElementById('back');
    back.style.display = "none";
    ds.style.display = "none";
}
//确认打赏
function exception() {
    $.ajax({
        type: "POST",//数据发送的方式（post 或者 get）
        url: "/moyue/user/reward",//要发送的后台地址
        data: {
            virtualCurrency :$('#currency').text(),
            bookId:$('.bookid').val(),
        },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
        dataType:"JSON",
        success: function (data) {//ajax请求成功后触发的方法
            if(data.code==0){
                dsclose();
                $('.point').text("打赏成功");
                bssucc();
                setTimeout("bsclose()", 2000);
                location = location;
            }else {
                // $('.suffx').text()
                // suff();
                var datas=data.msg;
                var cc=[];
                cc=datas.split(",");
                $('.suffx').text(cc[0]);
                $('.suffs').text(cc[1])
                suff();
            }
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("网络错误");//弹出错误信息
        }
    });
}
//打赏余额不足
function suff() {
    $('.not-suff').addClass('block').removeClass('none');
    $('.suffback').addClass('block').removeClass('none');
}
function nosuff() {
    $('.not-suff').addClass('none').removeClass('block');
    $('.suffback').addClass('none').removeClass('block');
}
//加入书架成功提醒
function bssucc() {
    $('.bookshelf').removeClass('none').addClass('block');
}
function bsclose() {
    $('.bookshelf').removeClass('block').addClass('none');
}
//加入书架
function bookrack() {
    $.ajax({
        type: "GET",//数据发送的方式（post 或者 get）
        url: "/moyue/user/jundgeuser",//要发送的后台地址
        data: {

        },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
        dataType:"JSON",
        async: false,
        success: function (data) {//ajax请求成功后触发的方法
            if(data.code==0){
                $.ajax({
                    type: "GET",//数据发送的方式（post 或者 get）
                    url: "/moyue/book/Addbookcase",//要发送的后台地址
                    data: {
                        bookId:$('.bookid').val(),
                    },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
                    dataType:"JSON",
                    success: function (data) {//ajax请求成功后触发的方法
                        bssucc();
                        setTimeout("bsclose()", 2000);
                        location = location;
                    },
                    error: function (msg) {//ajax请求失败后触发的方法
                        alert("网络错误");//弹出错误信息
                    },
                });
            }else {
                upup();
            }
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("网络错误");//弹出错误信息
        }
    });
}
//充值时是否登录
function yesuser() {
    $.ajax({
        type: "GET",//数据发送的方式（post 或者 get）
        url: "/moyue/user/jundgeuser",//要发送的后台地址
        data: {
        },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
        dataType:"JSON",
        success: function (data) {//ajax请求成功后触发的方法
            if (data.code ==  0){
                location= "/moyue/user/payment";
            }else{
                alert('请先登录');
                upup();
            }
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("网络错误");//弹出错误信息
        }
    });
}
//登录
function upup(){
    var denglu = document.getElementById('denglu');
    var back = document.getElementById('back');
    back.style.display = "block";
    denglu.style.display = "block";
}
function closes(){
    var denglu = document.getElementById('denglu');
    var back= document.getElementById('back');
    back.style.display = "none";
    denglu.style.display = "none";
}
function login() {
    $.ajax({
        type: "POST",//数据发送的方式（post 或者 get）
        url: "/moyue/login/loginUI",//要发送的后台地址
        data: {
            identity : $('#user').val(),
            certificate : $('#password').val(),
        },//要发送的数据（参数）格式为{'val1':"1","val2":"2"}
        dataType:"JSON",
        success: function (data) {//ajax请求成功后触发的方法
            console.log(data);
            if(data.code==0){
                closes();
                location.reload();
            }else{
                alert(data.msg);
            }
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("网络故障");//弹出错误信息
        }
    });
}
$(document).ready(function () {
    $("#li2").click(function () {
        $("#works").hide();
        $(".middle-wrap").fadeIn();
        $("#li2").css('color','#ed4259');
        $("#li1").css('color','#3F5A93');
    });
    $("#li1").click(function () {
        $("#works").fadeIn();
        $(".middle-wrap").hide();
        $("#li2").css('color','#3F5A93');
        $("#li1").css('color','#ed4259');
    });
    $(".short").click(function () {
        $(".book1").fadeIn();
        $("#fatie").hide();
        $("#list1").fadeOut();
        $("#list2").fadeIn();
        $(".lang").css("color", "#999");
        $(".short").css("color", "#000");
    })
    $(".lang").click(function () {
        $(".book1").hide();
        $("#fatie").fadeIn();
        $("#list1").fadeIn();
        $("#list2").fadeOut();
        $(".short").css("color", "#999");
        $(".lang").css("color", "#000");
    })
    getElem("autor1").innerHTML = getElem("autor").innerHTML;
});
function getElem(id) {
    return document.getElementById;
}
$(document).ready(function () {
    $("#fatie").click(function () {
        $(".post-text").fadeIn();
    })
    $("#delete-l").click(function () {
        $(".post-text").fadeOut();
    })
})


