//作家专区
//温馨提示
$(document).ready(function () {
//作品标签
    $('.section4').click(function(){
        $('#back1').css({'display':'block'});
    })
    $("#xiaoshi1").click(function(){
        $('#back1').css({'display':'none'});
    })

    $('#sure').click(function(){
        $('#back1').css({'display':'none'});

    })
    $(function(){
        $('.tagfilter span').click(function(){
            $(this).parent().each(function () {//移除其余非点中状态
                $('.tagfilte span').removeClass("special_color");
            });
            $(this).addClass("special_color");//给所点中的增加样式
            // console.log($(this).text());//输出所点的a的内容
            $("#tagg").val($(this).text());
            // console.log($("#tagg").val());
            $('#taggid').val($(this).siblings("input").val());
        })
    })
//	第一步
    $('#step1').click(function(){
        var list= $('input:radio[name="sex"]:checked').val();
        if(list==null){
            alert("请选中一个!");
            return false;
        }
        else{
            $('.ulist-step1').addClass('none');
            $('.ulist-step2').removeClass('none');
        }

    })
//	第二步
    $("#step2").click(function(){
        if($('#bookName').val() == "" ||  $('#introduce').val() == "" || $('#tagg').val() == ""){
            alert("请将作品信息填写完整！ ");
        }else{
            $('#back').css({'display':'block'});
        }
    })
    $('#step-back').click(function(){
        $('.ulist-step1').removeClass('none');
        $('.ulist-step2').addClass('none');
    })
    $('#tep1').click(function(){
        $('.ulist-step2').addClass('none');
        $('.ulist-step3').removeClass('none');
    })

    $("#xiaoshi").click(function(){
        $('#back').css({'display':'none'});
    })
//修改作品设置
    $('#step3').click(function(){
        $('.work-infom ul li dt').removeClass('none');
        $('.save').addClass('none');
    })
// 修改作品设置 确定
    $("#modify").click(function () {
        $('.work-infom ul li dt').addClass('none');
        $('.save').removeClass('none');
    })
})
//	作品名称
function work_title() {
    var reg = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;
    if (!(reg.test($('#ulist1').val()))) {
        $('#warm1').text("注意:作品名称只能输入中文、英文、数字且在15字以内");
        return false;
    } else {
        $('#warm1').text("OK");
        return true;
    }
}
var listarr = [];
function listchoose() {

    var typea = $('.browsers-id').val();
    var typeb = $('.browserson-id').val();
    var label = $('#taggid').val();
    listarr.push(typea, typeb, label);
    $('.browid').val(listarr);
}
// 作者写作
function works() {
    console.log($("#booklist").val());
    console.log($(".w-e-text").text());

    $.ajax({
        type:"POST",
        url:"/moyue/author/write",
        data:{
            booklist:$("#booklist").val(),
            content:$(".w-e-text").text(),
        },
        datatype:"JSON",
        success: function (data) {
            if(data.code==0){

            }
        },
    });
}
// 作品创建
function workadd() {
    listchoose();
    console.log($('#bookName').val());
    $.ajax({
        type:"POST",
        url:"/moyue/author/add",
        data:{
            bookName:$('#bookName').val(),
            bookIsContract:$('#contract').val(),
            bookDiscription:$('#introduce').val(),
            types:$('.browid').val(),
        },
        datatype:"JSON",
        success: function (data) {
            if(data.code==0){
                alert('书籍已成功创建！！')
            }
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("请稍后重试！！");//弹出错误信息
        }
    });
}
// 查看书籍详情
function workinfor() {
    $.ajax({
        type:"GET",
        url:"/moyue/message/{bookId}",
        data:"",
        datatype:"JSON",
        success: function (data) {
            if(data.code==0){
                $('#workname').html($('#bookName').val());
                $('#worktype').html("<span>listarr[0]</span> <span>listarr[1]</span>");
                $('#worklabel').html(listarr[2]);
                $('#workintro').html($('#introduce').val());
                $('#workcont').html($('#contract').val());
            }
        },
    });
}
workinfor();

//修改书籍信息
function workaddbook() {
    $.ajax({
        type:"POST",
        url:"/moyue/book",
        data:{
            bookName:$('#bookName1a').val(),
            bookIsContract:$('#contracta').val(),
            bookDiscription:$('#introducea').val(),
            types:listarr,
        },
        datatype:"JSON",
        success: function (data) {
            if(data.code==0){

            }
        },
    });
}