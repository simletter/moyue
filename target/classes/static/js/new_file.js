//      返回顶部
function pageScroll() {
    window.scrollBy(0,-10); //scrollBy() 方法可把内容滚动指定的像素数
    scrolldelay = setTimeout('pageScroll()',1);
    if(document.documentElement.scrollTop + document.body.scrollTop == 0){
        clearTimeout(scrolldelay); //使用 clearTimeout() 方法来阻止函数的执行
    }
}
window.onscroll = function () {
    if (document.documentElement.scrollTop + document.body.scrollTop > 0) {
        $('.return').fadeIn();
    }
    else {
        $('.return').fadeOut();
    }
}
// 点击书架用户是否登录
function getElem(id){
	return document.getElementById(id);
}
$(document).ready(function(){

    $(".amend").click(function(){
        $("#userName").attr("readonly",false);
    })
    $(".amend1").click(function(){
        $("#userSex").attr("readonly",false);
    })

    $('#phone-binding').click(function(){
        $('.all2').fadeIn();
        $('.phone').animate({height:'360px'});
    })
    $('#email-binding').click(function(){
        $('.all2-c').fadeIn();
        $('.email').animate({height:'360px'});
    })
    $('#fork').click(function(){
        $('.all2').fadeOut();
        $('#phone-info').val('');
        $('.wain').html("");
        $('.phone').animate({height:'0'});
    })
    $('#fork-c').click(function(){
        $('.all2-c').fadeOut();
        $('#email-info').val('');
        $('.wains').html("");
        $('.email').animate({height:'0'});
    })
})
// 绑定手机
function  checking(){
	if(!(/^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/.test($('#phone-info').val()))){
        $('.wain').text('手机格式不正确，请重新输入！！！');
    }else{
        $('.wain').text('OK,可绑定！~');
        $('.tel-btn').click(function(){
            $('.all2').fadeOut();
            $('.phone').animate({height:'0px'});
        })
    }
}
// 绑定邮箱
function email(){
    if(!(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test($('#email-info').val()))){
        $('.wains').html("邮箱格式错误");
    }else {
        $('.wains').html("OK,可绑定！~");
        $('.tel-btns').click(function(){
            $('.all2-c').fadeOut();
            $('.email').animate({height:'0px'});
        })
    }
}
/*input 上传图片*/
//在input file内容改变的时候触发事件
$('#exampleInputFile').change(function(){
    //获取input file的files文件数组;
    //$('#filed')获取的是jQuery对象，.get(0)转为原生对象;
    //这边默认只能选一个，但是存放形式仍然是数组，所以取第一个元素使用[0];
    var file = $('#exampleInputFile').get(0).files[0];
    //创建用来读取此文件的对象
    var reader = new FileReader();
    //使用该对象读取file文件
    reader.readAsDataURL(file);
    //读取文件成功后执行的方法函数
    reader.onload=function(e){
        //读取成功后返回的一个参数e，整个的一个进度事
        //选择所要显示图片的img，要赋值给img的src就是e中target下result里面
        //的base64编码格式的地址
        $('#imgshow').get(0).src = e.target.result;
    }
})


//个人中心
$(document).ready(function(){
//	我的书架
	$("#section1").click(function(){
		$("#search").fadeOut();
		$("#group2").fadeOut();
		$("#group1").fadeIn();		
	});
	
	$("#section2").click(function(){
		$("#search").fadeIn();
		$("#group2").fadeOut();
		$("#group1").fadeIn();		
	});
	
	$("#section3").click(function(){
		$("#search").fadeOut();
		$("#group1").fadeOut();
		$("#group2").fadeIn();
	});
//	消息中心
	$("#aa").click(function(){
		$("#a1").fadeIn();
		$("#b1").fadeOut();
	});
	$("#bb").click(function(){
		$("#b1").fadeIn();
		$("#a1").fadeOut();
	});
//	新建分组
	$("#ccc").click(function(){
		$(".new-grouping").removeClass('newactive')
		$(".new-grouping").addClass('returnB');
		$(".new-grouping").fadeOut();
	});
	$("#new-group").click(function(){
			$(".new-grouping").fadeIn();
			$(".new-grouping").removeClass('returnB');
			$(".new-grouping").addClass('newactive');
			
		});
	});

//创建新分组
		$(document).ready(function(){
			$(".new-grouping-creation").click(function(){
				if(getElem("creat-grouping").value == ""){
					$(".a").show();
				}else{
					$(".a").hide();
					var elem_li = document.createElement('li'); // 生成一个 li元素
			        elem_li.innerHTML = getElem("creat-grouping").value; // 设置元素的内容
			        getElem('ul1').appendChild(elem_li); // 添加到UL中去
			        var logo=document.createElement('span');
			        logo.style.float = "right";
			        logo.style.padding="0 12px 0 0";
			        logo.className='iconfont';
			        logo.innerHTML="&#xe608;" + "&#xe626;";
			        elem_li.appendChild(logo);
				}
			});
		});
//字数限制
		function setShowLength(obj, maxlength, id){ 
			var rem = maxlength - obj.value.length; 
			var wid = id; 
			if (rem < 0){ 
				rem = 0; 
			} 
			getElem(wid).innerHTML =  rem + "/8"; 
		} 
//日历
		!function(){
			laydate.skin('molv');//切换皮肤，请查看skins下面皮肤库
			laydate({elem: '#demo'});//绑定元素
			}();
			//日期范围限制
			var start = {
			    elem: '#start',
			    format: 'YYYY-MM-DD',
			    min: laydate.now(), //设定最小日期为当前日期
			    max: '2099-06-16', //最大日期
			    istime: true,
			    istoday: false,
			    choose: function(datas){
			         end.min = datas; //开始日选好后，重置结束日的最小日期
			         end.start = datas //将结束日的初始值设定为开始日
			    }
			};
			var end = {
			    elem: '#end',
			    format: 'YYYY-MM-DD',
			    min: laydate.now(),
			    max: '2099-06-16',
			    istime: true,
			    istoday: false,
			    choose: function(datas){
			        start.max = datas; //结束日选好后，充值开始日的最大日期
			    }
			};
			laydate(start);
			laydate(end);
			//自定义日期格式
			laydate({
			    elem: '#test1',
			    format: 'YYYY年MM月DD日',
			    festival: true, //显示节日
			    choose: function(datas){ //选择日期完毕的回调
			        alert('得到：'+datas);
			    }
			});
			//日期范围限定在昨天到明天
			laydate({
			    elem: '#hello3',
			    min: laydate.now(-1), //-1代表昨天，-2代表前天，以此类推
			    max: laydate.now(+1) //+1代表明天，+2代表后天，以此类推
			});

