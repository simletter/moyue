function safetyss() {
    $.ajax({
        url:"/moyue/login/security",
        type:"GET",
        data:"",
        datatype:"json",
        success: function (data) {
            if(data.code == 0){
                var datas = data.data;
                console.log(datas);
                for (var h in datas){
                    if(datas[h].loginType == 'phone'){
                        $("#phone-binding").text("更换手机");
                        $("#phone-binding").css({"background-color":"#fff","color":"#2985F6"});
                        $("#phone-h3").html("已绑定手机，<span id='tel' th:text='${datas[h].identity}'></span> 已开启手机号登录");
                    }
                    if(datas[h].loginType == 'email'){
                        $("#email-binding").text("更换邮箱");
                        $("#email-binding").css({"background-color":"#fff","color":"#2985F6"});
                        $("#email-h3").html("已绑定邮箱，<span class='e-mai' th:text='${datas[h].identity}'></span> 已开启邮箱号登录");
                    }
                    if(datas[h].loginType =='github'){
                        $("#github-binding").text("更换Github");
                        $("#github-binding").css({"background-color":"#fff","color":"#2985F6"});
                        $("#github-h3").html("已绑定Github，<span th:text='${datas[h].identity}'></span> 已开启Github号登录");
                    }
                }

            }
        }//ajax请求成功后触发的方法
    });
}
safetyss();

// 绑定手机
function bindphone() {
    $.ajax({
        url:"/moyue/login/binding/phone",
        type:"POST",
        data:{
            phone:$('#phone-info').val(),
        },
        datatype:"JSON",
        success: function (data) {

        }
    })
}
// 绑定邮箱
function bindemail() {
    $.ajax({
        url:"",
        type:"GET",
        data:{
            loginType:$('#email-info').val(),
        },
        datatype:"JSON",
        success: function (data) {

        }
    })
}
// 用户头像
function imgshow() {
    $.ajax({
        url:"",
        type:"",
        data:"",
        datatype:"JSON",
        success:function (data) {
            if (data.code == 0){
                $('.imgShow').html('');
            }
        }

    })
}
imgshow();