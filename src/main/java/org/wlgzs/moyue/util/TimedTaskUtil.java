package org.wlgzs.moyue.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.wlgzs.moyue.dao.ActivityDao;
import org.wlgzs.moyue.dao.BookDao;
import org.wlgzs.moyue.entity.Activity;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.service.BookService;

import java.util.List;

@Component
public class TimedTaskUtil {
    @Autowired
    private BookDao bookDao;
    @Autowired
    private ActivityDao activityDao;
    @Autowired
    private BookService bookService;

    @Scheduled(fixedRate = 24 * 60 * 60 * 1000)
    public void run() {
        activityDao.deleteAll();
        List<Book> books = bookDao.getBooksByBookIsContract(1);
        int n = books.size(), randomNum;
        for (int i = 0; i < 10; i++) {
            for (; activityDao.count() < 10; ) {
                randomNum = (int) (Math.random() * n);
                Activity activity = new Activity();
                activity.setBookId(books.get(randomNum).getBookId());
                activityDao.save(activity);
                System.out.println(activityDao.count());
            }
        }
        bookService.removeBooksClick();
        bookService.removeBooksCollections();
        bookService.removeRankBook();
        bookService.removeBooksreward();
        bookService.rankBook();
        bookService.getBooksreward();
        bookService.getBooksCollections();
        bookService.getBooksClick();
    }

}
