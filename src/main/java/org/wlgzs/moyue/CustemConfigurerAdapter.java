package org.wlgzs.moyue;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wlgzs.moyue.filter.AdminFilter;
import org.wlgzs.moyue.filter.LoginFilter;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class CustemConfigurerAdapter {
    @Bean
    public FilterRegistrationBean authFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("AdminFilter");
        AdminFilter adminFilter = new AdminFilter();
        registrationBean.setFilter(adminFilter);
        registrationBean.setOrder(1);
        List<String> urlList = new ArrayList<String>();
        urlList.add("/moyu");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;

    }
    @Bean public FilterRegistrationBean LoginFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("LoginFilter");
        LoginFilter loginFilter = new LoginFilter();
        registrationBean.setFilter(loginFilter);
        registrationBean.setOrder(2);
        List<String> urlList = new ArrayList<String>();
        urlList.add("/moy");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;
    }

}
