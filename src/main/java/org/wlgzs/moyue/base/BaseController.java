package org.wlgzs.moyue.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.wlgzs.moyue.dao.*;
import org.wlgzs.moyue.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class BaseController {

    @Autowired
    protected UserService userService;
    @Autowired
    protected UserDao userDao;
    @Autowired
    protected LoginDao loginDao;
    @Autowired
    protected LoginService loginService;
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected HttpSession session;
    @Autowired
    protected BookDao bookDao;
    @Autowired
    protected BookService bookService;
    @Autowired
    protected BookLinkTypeService    bookLinkTypeService;
    @Autowired
    protected SensitiveWordService sensitiveWordService;
    @Autowired
    protected CommentServicee commentServicee;
    @Autowired
    protected BookListService bookListService;
    @Autowired
	protected BookLinkTypeDao bookLinkTypeDao;
	@Autowired
	protected TypeDao typeDao;
	@Autowired
	protected BookListDao bookListDao;
	@Autowired
	protected BookcaseDao bookcaseDao;
	@Autowired
	protected BookcaseService bookcaseService;
	@Autowired
	protected ConsumeService consumeService;
	@Autowired
    protected ConsumeDao consumeDao;
	@Autowired
    protected MessageService messageService;
	@Autowired
    protected TypeService typeService;
	@Autowired
    protected ActivityService activityService;
	@Autowired
    protected BrowseService browseServicek;

}