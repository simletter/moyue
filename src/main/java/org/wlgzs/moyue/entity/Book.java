package org.wlgzs.moyue.entity;
/*
* 书籍表
* 用于对书籍个人信息的记录
*
 *  */
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Data
@Table(name = "t_book")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long bookId;                         //ID
    private String bookName;                     //书名
    private long authorId;                       //作者ID
    private String authorName;                   //作者名字
    private String bookDescription;              //简介
    private String bookCoverUrl;                 //书籍封面URL
    private String bookUrl;                      //书籍文件URL，为整本书的地址，可用于下载
    private int bookIsActivity;                  //活动状态:(进行中-0，无活动-1)
    private int bookIsContract;                  //签约状态:(已签约1/未签约0)
    private int bookIsReview;                    //审核状态(通过1/不通过0)
    private int bookIsEnd;                       //完结状态:(完结1/连载0)
    private Date bookUpdateDate;                 //最后更新时间
    private int bookWordCount;                   //总字数
    private int bookListCount;                   //章节数量
    private int bookClicks;                      //点击量
    private int bookCollections;                 //收藏量
    private int bookRewards;                     //打赏虚拟币量
    private long activityId;                     //活动ID
}
