package org.wlgzs.moyue.entity;
/*
 *敏感词汇管理表
 * */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "t_sensitiveword")
@NoArgsConstructor
@AllArgsConstructor
public class SensitiveWord  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long wordId;                         //敏感词ID
    private String sensitiveWord;                  //敏感词
}
