package org.wlgzs.moyue.entity;

/*
* 活动管理表
* 用于管理员添加活动
* */

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
@Data
@Entity
@Table(name = "t_activity")
@NoArgsConstructor
@AllArgsConstructor
public class Activity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long activityId;                         //参加活动ID·
    private long bookId;                             //书籍ID
}
