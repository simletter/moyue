package org.wlgzs.moyue.entity;
/*
 *书籍分组管理表
 * */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "t_bookcase")
@NoArgsConstructor
@AllArgsConstructor
public class Bookcase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;                         //表ID
    private long groupId;                    //书籍分组ID==用户ID
    private long bookId;                     //书籍ID
    private int isShow;                      //是否在书架显示

}
