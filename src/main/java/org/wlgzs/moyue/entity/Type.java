package org.wlgzs.moyue.entity;
/*
 *分类管理表
 * */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name = "t_booktype")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Type implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long typeId;                          //类型ID/标签ID
    private String typeName;                      //类型名字/标签名字
    private Long typeParentId;                    //类型父ID/标签父ID为-1
}
