package org.wlgzs.moyue.entity;
/*
 * 书籍与分类 多对多关系表
 * */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "t_booklinktype")
@NoArgsConstructor
@AllArgsConstructor
public class BookLinkType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;                         //ID
    private long bookId;                     //书籍ID
    private long typeId;                     //书籍所属分类的ID
}
