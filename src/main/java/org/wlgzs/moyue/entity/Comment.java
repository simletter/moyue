package org.wlgzs.moyue.entity;
/*
 *评论记录表
 * */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "t_comment")
@NoArgsConstructor
@AllArgsConstructor
public class Comment  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long commentId;                        //评论ID
    private long userId;                           //用户ID
    private long bookId;                           //书籍ID
    private String commentContent;                 //评论内容
    private Date commentDate;                      //评论时间
}
