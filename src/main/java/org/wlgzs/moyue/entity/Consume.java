package org.wlgzs.moyue.entity;
/*
 *消费记录表
 * */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "t_consume")
@NoArgsConstructor
@AllArgsConstructor
public class Consume implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long consumeId;                     //消费记录ID
    private String consumeType;                 //消费类型
    private long userId;                        //用户ID
    private long consumeObjectId;               //消费对象ID
    private long consumeMoney;                  //消费虚拟币金额
    private Date consumeDate;                   //消费日期
    private int isShow;                         //是否对用户显示
    private String consumeNote;                 //消费备注
}