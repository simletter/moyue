package org.wlgzs.moyue.filter;


import org.wlgzs.moyue.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        User user = (User) request.getSession().getAttribute("user");
        if (user.getRoleId().equals("4")) {
            request.getRequestDispatcher("/admin/bms").forward(request, response);
        } else {
            filterChain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {

    }
}
