package org.wlgzs.moyue.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Message;
import org.wlgzs.moyue.util.Result;

@RestController
@RequestMapping("/message")
public class MessageController extends BaseController {
    //分页分阅读状态获取信息
    @GetMapping("/all")
    public ModelAndView allMesssage(@RequestParam("page") int page, Model model, @RequestParam("isbrowse") int isBrowsse) {
        return messageService.allMessage(page, model, session, isBrowsse);
    }

    //删除信息
    @DeleteMapping
    public Result deleteMessage(int messageId) {
        return messageService.deleteMessage(messageId);
    }

    //批量删除信息
    @DeleteMapping("/deletes")
    public Result deleteMessages(long[] messaageIds) {
        return messageService.deleteMessages(messaageIds);
    }

    //添加信息
    @PostMapping
    public Result addMessage(Message message) {
        return messageService.addMessage(message, session);
    }

    //阅读信息
    @GetMapping
    public ModelAndView readMessage(long messageId,Model model){
        return messageService.readMessage(messageId,model);
    }


}
