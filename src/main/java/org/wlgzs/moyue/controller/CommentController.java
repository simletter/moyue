package org.wlgzs.moyue.controller;

import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Comment;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.service.servicelmpl.CommentImpl;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController extends BaseController {
    //用户添加对书籍评论
    @PostMapping
    public Result addComment(Comment comment) {
        return commentServicee.addComment(comment, session);
    }

    //用户个人页面评论的展示
    @GetMapping("/user")
    public ModelAndView showByUserId(@RequestParam("page") int page, Model model) {
        return commentServicee.commentsByUserId(page, session, model);
    }


    //评论分页的展示
    @GetMapping("/book")
    public ModelAndView showByBookId(@RequestParam("page") int page, long bookId, Model model) {
        return commentServicee.commentsByBookId(page, bookId, model);
    }



    //用户删除评论
    @DeleteMapping
    public Result deleteComment(long commentId) {
        return commentServicee.deleteComment(commentId, session);
    }

    //用户批量删除评论
    @DeleteMapping("/book")
    public Result deleteComment(long[] num) {
        return commentServicee.deleteComments(num, session);
    }
}
