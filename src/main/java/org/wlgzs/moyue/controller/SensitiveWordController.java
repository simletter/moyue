package org.wlgzs.moyue.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.SensitiveWord;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.util.List;

@RestController
@RequestMapping("/word")
public class SensitiveWordController extends BaseController {


    //增加敏感词
    @PostMapping
    public Result addSensitiveWord(String word) {
        return sensitiveWordService.addSensitiveWord(word);
    }

    //删除敏感词
    @DeleteMapping
    public Result deleteSensitiveWord(@RequestParam("wordid") String wordid) {
        System.out.println("============================"+wordid);
        long wordId=Long.parseLong(wordid);
        return sensitiveWordService.deleteSensitiveWord(wordId);
    }

    //无序分页显示敏感词
    @GetMapping("/all")
    public ModelAndView showSensitiveWord(@RequestParam("page") String page, Model model) {
        return sensitiveWordService.showAllSensitiveWords(page, model);
    }
}
