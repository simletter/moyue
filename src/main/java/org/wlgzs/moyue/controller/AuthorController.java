package org.wlgzs.moyue.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.entity.BookList;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

@RestController
@RequestMapping("/author")
public class AuthorController extends BaseController {
    //获得书籍详情
    @GetMapping("/message/{bookId}")
    public Result bookMessage(@PathVariable("bookId") long bookId) {
        return new Result(ResultCode.SUCCESS, bookService.getBookMessage(bookId));
    }

    //作者写作
    @PostMapping("/write")
    public Result writeBook(BookList bookList, String content) {
        if (bookService.writeBook(bookList, content)) return new Result(ResultCode.SUCCESS, "章节上传成功!");
        else return new Result(ResultCode.FAIL, "章节上传失败，请重试！");
    }

    //书籍创建
    @PostMapping("/add")
    public Result addBook(MultipartFile file, Book book, int[] types) {
        bookService.addBook(file, book, types);
        return new Result(ResultCode.SUCCESS, "书籍已成功创建！");
    }

    //创作页面获得新章节指定信息
    @GetMapping("/writehtml/{bookId}")
    public Result writeBookHtml(@PathVariable("bookId") long bookId) {
        return bookService.getBooklist(bookId);
    }

    @GetMapping("/build")
    public Result tuAuthor() {
        User user = (User) session.getAttribute("user");
        user.setRoleId("2");
        userDao.save(user);
        return new Result(ResultCode.SUCCESS);
    }
}
