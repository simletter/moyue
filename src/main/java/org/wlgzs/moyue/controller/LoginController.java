package org.wlgzs.moyue.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Login;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/login")
public class LoginController extends BaseController {

    //站内登陆
    @PostMapping(value = "loginUI")
    public Result insideLogin(Login login) {
        return loginService.insideLogin(login, session);
    }

    //解绑账号
    @GetMapping(value = "/untie/{loginId}")
    public Result untieLogin(@PathVariable("loginId") long loginId) {
        return loginService.deleteLogin(loginId, session);
    }


    @GetMapping(value = "gitCallBack.do")
    public ModelAndView gitLogin(String code, Model model) throws IOException {
        Login login = new Login();
        String identity = loginService.githubLogin(code);
        User user = (User) session.getAttribute("user");
        return loginService.githubJudge(identity, session, "github", model);
    }

    @PostMapping("/binding/phone")
    public Result addPhone(String phone) {
        return loginService.addPhone(phone, session);
    }


    @GetMapping(value = "/security")
    public Result showLogins() {
        return loginService.showLoginMessage(session);
    }

    @PostMapping(value = "user/forgetPwdU")
    public Result addPassword(Login login) {
        System.out.println("asdfjsdfjd  " + login.getIdentity());
        String identify = login.getIdentity();
        Login login1 = loginDao.findByIdentity(identify);
        Long id = login1.getUserId();
        System.out.println("qwnklfweqkljfeopwgj" + id);
        loginDao.updatePassword(login.getCertificate(), id);
        return new Result(ResultCode.SUCCESS);
    }


}
