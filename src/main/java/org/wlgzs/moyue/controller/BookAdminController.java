package org.wlgzs.moyue.controller;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.entity.Type;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.util.List;

/**
 * @Auther: dayfly
 * @Date: 2018/4/22 14:55
 * @Description:
 */
@RestController
@RequestMapping("/admin/book")
public class BookAdminController extends BaseController {
    //展示活动书籍
    @GetMapping("/activity")
    public Result getActivityBook() {
        return activityService.showActivity();
    }

    //分页显示搜索所有书籍
    @GetMapping("/all")
    public ModelAndView getBookAll(@RequestParam("page") int page, Model model) {
        return bookService.getAllBook(page, model);
    }

    //书籍上传
    @PostMapping("/upload")
    public Result uploadBook(MultipartFile bookfile, MultipartFile imgfile, Book book, @RequestParam("types") int[] types) {
        User user = (User) session.getAttribute("user");
        return bookService.uploadBook(bookfile, imgfile, book, user, types);
    }

    //根据ID删除书籍
    @DeleteMapping("/book/{bookId}")
    public Result deleteBook(@PathVariable("bookId") long bookId) {
        return bookService.deleteBook(bookId);
    }

    //批量删除书籍
    @DeleteMapping("/books")
    public Result deleteBooks(long[] bookIds) {
        return bookService.deleteBooks(bookIds);
    }

    @GetMapping("/uploadUI")
    public ModelAndView toUpload(Model model) {
        List<Type> typeList = typeDao.findByTypeParentId((long) 0);
        List<Type> typeList1 = typeDao.findByTypeParentId((long) -1);
        model.addAttribute("typelist", typeList);
        model.addAttribute("label", typeList1);
        return new ModelAndView("uploadbook");
    }

    @PostMapping("/getson")
    public Result getson(@RequestParam("typeId") long typeId) {
        List<Type> typeList = typeDao.findByTypeParentId(typeId);
        return new Result(ResultCode.SUCCESS, typeList);
    }

    //查看书籍详情
    @GetMapping("/message")
    public Result getBookMesssage(@RequestParam("bookId") long bookId) {
        return bookService.getBookMessage(bookId);
    }

    //修改书籍信息
    @PutMapping("/book")
    public Result updateBook(Book book) {
        return bookService.updateBook(book);
    }
}
