package org.wlgzs.moyue.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.entity.Bookcase;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/book")
public class BookcaseController extends BaseController {
    @GetMapping("/Addbookcase")
    public Result addBookCase(@RequestParam("bookId") long bookId){
            User user = (User) session.getAttribute("user");
            return bookcaseService.addBookCase(user,bookId);
    }
    @GetMapping("/bookcaselist")//用户进入我的书架
    public ModelAndView bookCaseList(Model model){
            Book book = new Book();
            User user = (User)session.getAttribute("user");
            bookcaseService.bookCaseList(model,user);
            List<Book> rankList= bookService.rankBook();
            model.addAttribute("commandBook",bookService.recommend(user.getUserId(),rankList));
        return new ModelAndView("personal");
        }
    @GetMapping("/deletebookcase/{bookId}")//用户将书移除书架
    public ModelAndView deleteBookCase(Model model,@PathVariable("bookId") Long bookId) {
        User user = (User) session.getAttribute("user");
        bookcaseService.deleteBookCase(model, user, bookId);
        return new ModelAndView("personal");
    }
}
