package org.wlgzs.moyue.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.service.BrowseService;
import org.wlgzs.moyue.util.Result;

@Controller
@RequestMapping("/browse")
public class BrowseController extends BaseController {
    @Autowired
    private BrowseService browseService;

    //删除浏览记录
    @DeleteMapping
    public Result deleteBrowse(long browseId) {
        return browseService.deleteBrowse(browseId, session);
    }

    //获取用户主页个人浏览记录
    @GetMapping("/all")
    public ModelAndView getAllBrowse(@RequestParam("page") int page, Model model) {
        return browseService.findBrowse(page, session, model);
    }

    //获取书籍页面个人书签
    @GetMapping("/bookmarks")
    public Result getBookmarks(long bookId) {
        return browseService.findBookmark(bookId, session);
    }


}
