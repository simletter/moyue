package org.wlgzs.moyue.controller;

import com.alipay.api.*;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.base.BaseController;
import org.wlgzs.moyue.entity.Consume;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/user")
public class PaymentController extends BaseController {
    @GetMapping(value = "/payment")
    public ModelAndView payment() {
        return new ModelAndView("pay");
    }

    @PostMapping(value = "/pay")
    public ModelAndView pay(HttpServletRequest request, HttpServletResponse response) throws AlipayApiException, IOException {
        User user = (User)session.getAttribute("user");
        userService.payment(request, response);
        return new ModelAndView("financial");
    }
    @PostMapping(value = "paychapter")
    public ModelAndView paychapter(Model model){
        User user = (User)session.getAttribute("user");
        model.addAttribute("usermoney",user.getUserRewards());
        return new  ModelAndView("subscription");
    }
    @GetMapping(value = "/returnurl")
    public ModelAndView yongHu(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException, AlipayApiException, ParseException {
        userService.returnUrl(request, response, session);
        User user = (User)session.getAttribute("user");
        model.addAttribute("msg", "支付成功");
        return new ModelAndView("redirect:/moyue/user/user/financl");

    }
    @PostMapping("/buychapter")//用户购买章节
    public Result buyChapter(@RequestParam("chapters") Long[] chapters) {
        User user = (User) session.getAttribute("user");
        Long userId = user.getUserId();
        String isOk = consumeService.buychapters(chapters, userId);
        if (isOk.equals("ok")) {
            return new Result(ResultCode.SUCCESS, "购买成功");
        }else {
            return new Result(ResultCode.FAIL,isOk);
        }
    }
    @PostMapping("/reward")//用户打赏
    public Result reward(@RequestParam("virtualCurrency") String virtualCurrency, @RequestParam("bookId") Long bookId) {
      return consumeService.jundgeReward(virtualCurrency,bookId,session);
    }
    @PostMapping("/forwordReword")//用户打赏前对用户余额进行查询
    public Result forwordReword(){
        User user = (User) session.getAttribute("user");
        User trueUser = userDao.findById(user.getUserId()).get();
        return new Result(ResultCode.SUCCESS,trueUser.getUserRewards());
    }
    @RequestMapping("/user/financl")//用户进入财务中心
    public ModelAndView financialhtml(Model model,User user){
        consumeService.showFinancial(model,session);
        return new ModelAndView("financial");
    }
    @DeleteMapping("/user/financl")
    public Result deleteConsume(@RequestParam("consumeId") long consumeId){
        consumeService.deleteConsume(consumeId);
        return new Result(ResultCode.SUCCESS,"删除成功");
    }
    @DeleteMapping("/financl")
    public Result deleteConsumes(@RequestParam("consumeId") Long[] consumesId){
        consumeService.deleteConsumes(consumesId);
        return new Result(ResultCode.SUCCESS,"删除成功");
    }
}
