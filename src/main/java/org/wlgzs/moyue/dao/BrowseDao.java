package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wlgzs.moyue.entity.Browse;

import java.util.List;

public interface BrowseDao extends JpaRepository<Browse,Long>,JpaSpecificationExecutor<Browse> {
    List<Browse> findBrowsesByUserId(long userId);
    List<Browse> getBrowsesByBookIdAndUserIdAndIsBookmarks(long bookId,long userId,int isBookarks);
}
