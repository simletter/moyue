package org.wlgzs.moyue.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.wlgzs.moyue.entity.BookList;

import java.util.List;

public interface BookListDao extends JpaRepository<BookList, Long> {
    List<BookList> getBookListsByBookId(long bookId);

    List<BookList> findByBookIdOrderByBookListOrderAsc(Long bookId);
    BookList findByBookIdAndBookListOrder(Long bookId,int listOrder);

    BookList findByBookListId(Long bookListId);

    int countByBookId(Long bookId);
}
