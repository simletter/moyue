package org.wlgzs.moyue.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.ui.Model;
import org.wlgzs.moyue.entity.Consume;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface ConsumeDao extends JpaRepository<Consume,Long> {
    Consume findByUserId(Long userId);
    List<Consume> findByConsumeTypeAndUserIdOrderByConsumeDateDesc(String string,Long userId);
    List<Consume> findByUserIdAndConsumeObjectIdAndConsumeType(Long userId,Long objectId,String consumeType);
    @Query("select p from Consume p where ( p.consumeType=?1 or p.consumeType=?2 ) and p.isShow=?3 and p.userId =?4 order by ?#{#sort}")
    List<Consume> getUserComsumes(String consumeType, String consumeTyp, int isShow,Long userId, Sort sort);
    List <Consume> findByConsumeTypeAndUserId(String consumeType,Long userId);
}
