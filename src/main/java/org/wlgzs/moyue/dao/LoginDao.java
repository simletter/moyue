package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.wlgzs.moyue.entity.Login;

import java.util.List;

public interface LoginDao extends JpaRepository<Login, Long> {
    Login findByIdentity(String identity);

    void deleteByUserId(Long userId);

    @Query("update Login o set o.identity=:identity where o.userId=:userId")
    void update(@Param("identity") String identity, @Param("userId") Long userId);

    @Transactional
    @Modifying
    @Query("update Login o set o.certificate =:certificate where o.userId =:userId")
    void updatePassword(@Param("certificate") String certificate, @Param("userId") Long userId);

    Login getLoginByIdentityAndCertificate(String identity, String certificate);

    List<Login> findByUserId(long userId);

    Login getLoginByUserIdAndLoginType(long userId, String loginType);
}
