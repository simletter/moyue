package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.wlgzs.moyue.entity.Activity;

/**
 * @Auther: dayfly
 * @Date: 2018/4/24 20:44
 * @Description:
 */
public interface ActivityDao extends JpaRepository<Activity,Long> {
    Activity getActivityByBookId(long bookId);
}
