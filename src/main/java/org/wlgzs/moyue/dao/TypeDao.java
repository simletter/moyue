package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.wlgzs.moyue.entity.Type;

import java.util.List;


public interface TypeDao extends JpaRepository<Type, Long> {
    List<Type> findByTypeParentId(Long parentId);

    Type findByTypeName(String typename);

    void deleteByTypeParentId(Long id);

     Type findByTypeIdAndTypeParentId(Long typeId, Long ParentId);



}
