package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.wlgzs.moyue.entity.User;

import java.util.List;


public interface UserDao extends JpaRepository<User, Long> {
    User getUserByUserId(long userId);

    User findByUserName(String username);

}