package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.wlgzs.moyue.entity.Comment;

import java.util.List;

/**
 * @Auther: dayfly
 * @Date: 2018/4/24 21:28
 * @Description:
 */
public interface CommentDao extends JpaRepository<Comment,Long>,JpaSpecificationExecutor<Comment> {
}
