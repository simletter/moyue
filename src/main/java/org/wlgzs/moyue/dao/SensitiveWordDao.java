package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wlgzs.moyue.entity.SensitiveWord;

import java.util.List;

public interface SensitiveWordDao extends JpaRepository<SensitiveWord, Long>, JpaSpecificationExecutor<SensitiveWord> {
    void deleteByWordId(Long wordId);
    List<SensitiveWord> findSensitiveWordsBySensitiveWordLike(String sensitiveWord);
}
