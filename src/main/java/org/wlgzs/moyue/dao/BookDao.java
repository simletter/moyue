package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.wlgzs.moyue.entity.Book;

import java.util.List;

/**
 * @Auther: dayfly
 * @Date: 2018/4/22 11:07
 * @Description:
 */
public interface BookDao extends JpaRepository<Book, Long>,JpaSpecificationExecutor<Book> {
    List<Book> getBooksByBookNameLike(String bookName);

    Book findBookByBookName(String bookName);

    List<Book> findByBookNameLike(String keyWord);

    List<Book> findAllByOrderByBookRewardsDesc();

    List<Book> findAllByOrderByBookClicksDesc();

    List<Book> findAllByOrderByBookCollectionsDesc();

    List<Book> findAllByBookNameContaining(String bookName);

    List<Book> getBooksByBookIsContract(int bookIsContract);

    Book getBookByBookName(String bookName);

    List<Book> findAllByAuthorNameContaining(String authorName);
    List<Book> findAllByBookIsEnd(int isEnd);

    List<Book> findAllByBookIsContract(int isFree);





    /*@Modifying
    @Query("select  *  from  t_book order by rand() limit 10,nativeQuery=true")
    List<Book> getActtivity();*/

}