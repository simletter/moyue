package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.wlgzs.moyue.entity.Bookcase;

import java.util.List;

public interface BookcaseDao extends JpaRepository<Bookcase,Long> {
    Bookcase findByBookIdAndGroupId(Long BookId,Long UserId);
    List<Bookcase> findByGroupIdAndIsShow(Long userId,int isshow);
    List<Bookcase> findByGroupId(Long userId);
    int countByGroupIdAndIsShow(Long userId,int isshow);
    @Modifying
    @Query("select  o.bookId  from  Bookcase o where o.groupId=?1 and o.isShow=?2")
    List<Long> getBooksId(Long userId,int isshow);
}