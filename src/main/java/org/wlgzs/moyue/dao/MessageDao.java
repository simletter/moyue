package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wlgzs.moyue.entity.Message;

import java.util.List;

public interface MessageDao extends JpaRepository<Message, Long>,JpaSpecificationExecutor<Message> {
    //根据用户Id获取审核信息表
    List<Message> getMessagesByUserId(Long userId);

    //根据阅读状态和用户ID获取信息表
    List<Message> getMessagesByUserIdAndIsBrowse(long userId, int isBrowse);

}
