package org.wlgzs.moyue.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.wlgzs.moyue.entity.BookLinkType;

import java.util.List;

public interface BookLinkTypeDao extends JpaRepository<BookLinkType, Long>,JpaSpecificationExecutor<BookLinkType> {
    void deleteByBookId(long bookId);

    List<BookLinkType> findBookLinkTypesByTypeId(long typeId);

    List<BookLinkType> findByBookId(Long bookId);

    @Query("select  o.typeId  from  BookLinkType o where o.bookId=?1")
    List<Long> getTypeIds(Long bookId);

    List<BookLinkType> findByTypeId(Long typeId);

    @Query("select  o.bookId  from  BookLinkType o where o.typeId=?1")
    List<Long> getBookIds(Long bookId);

}
