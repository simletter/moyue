package org.wlgzs.moyue.service;

import org.wlgzs.moyue.entity.BookList;
import org.wlgzs.moyue.util.Result;

public interface BookListService {
    //删除章节
    Result deleteBookList(long id);

    //更新目录
    Result updateBookList(BookList bookList);
}
