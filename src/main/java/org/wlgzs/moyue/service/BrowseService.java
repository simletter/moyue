package org.wlgzs.moyue.service;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.entity.Browse;
import org.wlgzs.moyue.util.Result;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface BrowseService {
    //添加浏览记录
    boolean addBrowse(long booklistId,HttpSession session);

    //删除浏览记录
    Result deleteBrowse(long browseId, HttpSession session);

    //批量删除浏览记录
    Result deleteBrowwses(long[] browseIds, HttpSession session);

    //显示用户页浏览记录
    ModelAndView findBrowse(int page, HttpSession session, Model model);

    //显示书签
    Result findBookmark(long bookId, HttpSession session);
}
