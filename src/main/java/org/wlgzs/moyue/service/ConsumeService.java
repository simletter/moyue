package org.wlgzs.moyue.service;

import org.springframework.ui.Model;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;

import javax.servlet.http.HttpSession;

public interface ConsumeService {
    String reward(User user, Long bookId, int money);

    String buychapters(Long[] chapters, Long userId);

    boolean isBuyed(long chapterId, long bookId, HttpSession session);

    void showFinancial(Model model, HttpSession session);

    Result jundgeReward(String virtualCurrency, Long bookId, HttpSession session);//用户进行打赏的方法

    void deleteConsume(Long consumeId);
    //批量删除消费记录
    void deleteConsumes(Long[] consumesId);

}