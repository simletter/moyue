package org.wlgzs.moyue.service;

import org.springframework.ui.Model;
import org.wlgzs.moyue.entity.Type;

import java.util.List;


public interface TypeService {
    //查看所有父类型
    void parentype(Model model);
    //增加父类型
    void addParentype(Model model,String parentypeName);
    //更改父类型名字
    void typeName( Long id,String typeName);
    //查看子类型
    List<Type> sontypeView(Model model, Long id);
    //展示全部标签
    List<Type> lables();
    //删除标签
    void deleteLable(Long id);
    //更改标签名字
    void updateLable(Long id,String lableName);
}
