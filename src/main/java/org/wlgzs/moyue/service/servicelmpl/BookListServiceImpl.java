package org.wlgzs.moyue.service.servicelmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wlgzs.moyue.dao.BookListDao;
import org.wlgzs.moyue.entity.BookList;
import org.wlgzs.moyue.service.BookListService;
import org.wlgzs.moyue.util.IoUtil;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

@Service
public class BookListServiceImpl implements BookListService {
    @Autowired
    private BookListDao bookListDao;

    public Result deleteBookList(long id) {
        BookList bookList = bookListDao.getOne(id);
        bookListDao.deleteById(id);
        new IoUtil().deleteFile("." + bookList.getBookListUrl());
        return new Result(ResultCode.SUCCESS);
    }

    public Result updateBookList(BookList bookList) {
        bookListDao.save(bookList);
        return new Result(ResultCode.SUCCESS);
    }
}
