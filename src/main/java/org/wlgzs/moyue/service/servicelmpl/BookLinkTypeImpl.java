package org.wlgzs.moyue.service.servicelmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wlgzs.moyue.dao.BookDao;
import org.wlgzs.moyue.dao.BookLinkTypeDao;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.entity.BookLinkType;
import org.wlgzs.moyue.service.BookLinkTypeService;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookLinkTypeImpl implements BookLinkTypeService {
    @Autowired
    private BookLinkTypeDao bookLinkTypeDao;
    @Autowired
    private BookDao bookDao;

    public boolean addBookLinkType(long bookId, int[] types) {
        long n = bookLinkTypeDao.count();
        BookLinkType bookLinkType = new BookLinkType();
        bookLinkType.setBookId(bookId);
        for (int i = 0; i < types.length; i++) {
            bookLinkType.setTypeId(types[i]);
            bookLinkTypeDao.save(bookLinkType);
        }
        if (bookLinkTypeDao.count() == (n + types.length)) return true;
        else return false;
    }

    public boolean updateBookLinkType(long bookId, int[] types) {
        long n = bookLinkTypeDao.count();
        bookLinkTypeDao.deleteByBookId(bookId);
        if (bookLinkTypeDao.count() == (n - types.length))
            return this.addBookLinkType(bookId, types);
        else return false;
    }


    public Result findBooksByTypes(List<Long> types) {
        List<BookLinkType> bookLinkTypes = bookLinkTypeDao.findBookLinkTypesByTypeId(types.get(0));
        for (int i = 1; i < types.size(); i++) {
            for (int j = 0; j < bookLinkTypes.size(); j++) {
                if (bookLinkTypes.get(j).getTypeId() != types.get(i))
                    bookLinkTypes.remove(j);
            }
        }
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < bookLinkTypes.size(); i++) {
            books.add(bookDao.findById(bookLinkTypes.get(i).getBookId()).get());
        }
        return new Result(ResultCode.SUCCESS, books);
    }
}
