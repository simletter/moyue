package org.wlgzs.moyue.service.servicelmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.dao.BookDao;
import org.wlgzs.moyue.dao.BookcaseDao;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.entity.Bookcase;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.service.BookcaseService;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookcaseServiceImpl implements BookcaseService {
    @Autowired
    BookcaseDao bookcaseDao;
    @Autowired
    BookDao bookDao;
    @Override
    public boolean saveBookCase(Long userId, Long bookId) {
        Bookcase userBookcase = bookcaseDao.findByBookIdAndGroupId(bookId, userId);

        if (userBookcase == null) {
            Book book = bookDao.findById(bookId).get();
            Bookcase bookcase = new Bookcase();
            bookcase.setBookId(bookId);
            bookcase.setGroupId(userId);
            bookcase.setIsShow(1);
            int collection = book.getBookCollections();
            book.setBookCollections(collection + 1);
            bookDao.save(book);
            bookcaseDao.save(bookcase);
            return true;
        }
        if (userBookcase != null) {
            if (userBookcase.getIsShow() != 1) {
                userBookcase.setIsShow(1);
                bookcaseDao.save(userBookcase);
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public void bookcaseView(User user, Model model) {
        Book book = new Book();


        List<Bookcase> bookView = bookcaseDao.findByGroupId(user.getUserId());
        List<Book> bookList = new ArrayList<Book>();
        for (Bookcase bookcase : bookView
                ) {
            book = bookDao.findById(bookcase.getBookId()).get();
            bookList.add(book);
        }
        model.addAttribute("bookview", bookList);
    }
    @Override
    public Result addBookCase(User user,long bookId){
        Long userId = user.getUserId();
        boolean jundge = saveBookCase(userId, bookId);
        if (jundge) {
            return new Result(ResultCode.SUCCESS, "加入书架成功");
        } else {
            return new Result(ResultCode.FAIL, "该书已在书架中");
        }
    }
    @Override
    public  void bookCaseList(Model model,User user){
        Book book = new Book();
        List<Bookcase> bookView = bookcaseDao.findByGroupIdAndIsShow(user.getUserId(),1);
        List<Book> bookList = new ArrayList<Book>();
        for (Bookcase bookcase:bookView
                ) {
            book = bookDao.findById(bookcase.getBookId()).get();
            bookList.add(book);
        }
        model.addAttribute("booklist",bookList);
    }
    @Override
     public void deleteBookCase(Model model,User user,Long bookId){
        Long userId = user.getUserId();
        Bookcase bookcase = bookcaseDao.findByBookIdAndGroupId(bookId,userId);
        if (bookcase==null){
            return;
        }
        bookcase.setIsShow(0);
        bookcaseDao.save(bookcase);
        System.out.println(bookcase);
        Book book = new Book();
        List<Bookcase> bookView = bookcaseDao.findByGroupIdAndIsShow(userId, 1);
        List<Book> bookList = new ArrayList<Book>();
        for (Bookcase bookcase1 : bookView
                ) {
            book = bookDao.findById(bookcase1.getBookId()).get();
            bookList.add(book);
        }
        model.addAttribute("booklist", bookList);
    }
}
