
package org.wlgzs.moyue.service.servicelmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wlgzs.moyue.dao.ActivityDao;
import org.wlgzs.moyue.dao.BookDao;
import org.wlgzs.moyue.entity.Activity;
import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.service.ActivityService;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: dayfly
 * @Date: 2018/4/24 20:43
 * @Description:
 */
@Service
public class ActivityImpl implements ActivityService {
    @Autowired
    private ActivityDao activityDao;
    @Autowired
    private BookDao bookDao;

    @Override
    public Result showActivity() {
        List<Activity> activities = activityDao.findAll();
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < activities.size(); i++) {
            books.add(bookDao.findById(activities.get(i).getBookId()).get());
        }
        return new Result(ResultCode.SUCCESS, books);
    }
}
