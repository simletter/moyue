package org.wlgzs.moyue.service.servicelmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.dao.*;
import org.wlgzs.moyue.entity.*;
import org.wlgzs.moyue.service.BrowseService;
import org.wlgzs.moyue.util.Result;
import org.wlgzs.moyue.util.ResultCode;

import javax.persistence.Access;
import javax.persistence.criteria.*;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Service
public class BrowseImpl implements BrowseService {
    @Autowired
    private BrowseDao browseDao;
    @Autowired
    private BookDao bookDao;
    @Autowired
    private BookListDao bookListDao;
    @Autowired
    private BookLinkTypeDao bookLinkTypeDao;
    @Autowired
    private TypeDao typeDao;

    @Override
    public boolean addBrowse(long booklistId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        BookList bookList = bookListDao.getOne(booklistId);
        Book book = bookDao.getOne(bookList.getBookId());
        BookLinkType bookLinkType = bookLinkTypeDao.findByBookId(book.getBookId()).get(0);
        Type type = typeDao.getOne(bookLinkType.getTypeId());
        Browse browse = new Browse(0, user.getUserId(), book.getAuthorId(), book.getAuthorName(), book.getBookId(), book.getBookName(), booklistId, bookList.getBookListOrder(), bookList.getBookListName(), type.getTypeId(), type.getTypeName(), new Date(), 0, 0);
        browseDao.save(browse);
        return true;
    }

    public Result deleteBrowse(long browseId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (browseDao.findById(browseId).get().getUserId() == user.getUserId()) {
            browseDao.deleteById(browseId);
            return new Result(ResultCode.SUCCESS);
        } else return new Result(ResultCode.FAIL);
    }

    public Result deleteBrowwses(long[] browseIds, HttpSession session) {
        User user = (User) session.getAttribute("user");
        for (int i = 0; i < browseIds.length; i++) {

            browseDao.deleteById(browseIds[i]);
        }
        return new Result(ResultCode.SUCCESS);
    }

    public Result findBookmark(long bookId, HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<Browse> browses = browseDao.getBrowsesByBookIdAndUserIdAndIsBookmarks(bookId, user.getUserId(), 1);
        if (browses.size() != 0) return new Result(ResultCode.SUCCESS, browses);
        else return new Result(ResultCode.FAIL, "还没有书签，快去动动手吧！");
    }

    public ModelAndView findBrowse(int page, HttpSession session, Model model) {
        User user = (User) session.getAttribute("user");
        Pageable pageable = new PageRequest(page - 1, 10);
        Specification<Browse> specification = new Specification<Browse>() {
            @Override
            public Predicate toPredicate(Root<Browse> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path path = root.get("userId");
                return criteriaBuilder.equal(path, user.getUserId());
            }
        };
        Page<Browse> browsePage = browseDao.findAll(specification, pageable);
        model.addAttribute("page", browsePage.getNumber() + 1);
        model.addAttribute("pages", browsePage.getTotalPages());
        model.addAttribute("browses", browsePage.getContent());
        return new ModelAndView("");
    }

}
