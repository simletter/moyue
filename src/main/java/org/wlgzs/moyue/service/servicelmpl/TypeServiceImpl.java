package org.wlgzs.moyue.service.servicelmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.wlgzs.moyue.dao.BookDao;
import org.wlgzs.moyue.dao.TypeDao;
import org.wlgzs.moyue.entity.Type;
import org.wlgzs.moyue.service.BookService;
import org.wlgzs.moyue.service.TypeService;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    private BookService bookService;
    @Autowired
    private TypeDao typeDao;
    @Autowired
    private BookDao bookDao;
    @Override
    public void parentype(Model model){
        List<Type> parenttypes = bookService.findtypeId((long) 0);

        model.addAttribute("parenttypes", parenttypes);
    }
    @Override
    public void addParentype(Model model,String parentypeName){
        Type type = new Type();
        type.setTypeName(parentypeName);
        type.setTypeParentId((long) 0);
        typeDao.save(type);
        List<Type> parenttypes = bookService.findtypeId((long) 0);
        model.addAttribute("parenttypes", parenttypes);
    }
    @Override
    public void typeName( Long id,String typeName){
        Type type = typeDao.findById(id).get();
        type.setTypeName(typeName);
        typeDao.save(type);
    }
    @Override
   public List<Type> sontypeView(Model model, Long id){
        List<Type> sontypes =typeDao.findByTypeParentId(id);
        return sontypes;
    }
    @Override
    public  List<Type> lables(){
        List<Type> lables = typeDao.findByTypeParentId((long) -1);
        return lables;
    }
    @Override
    public void deleteLable(Long id){
        typeDao.deleteById(id);
    }
    @Override
   public void updateLable(Long id,String lableName){
        Type type = typeDao.findById(id).get();
        type.setTypeName(lableName);
        type.setTypeParentId((long) -1);
        typeDao.save(type);
        List<Type> lables = typeDao.findByTypeParentId((long) -1);
    }
}
