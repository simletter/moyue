package org.wlgzs.moyue.service;

import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.entity.Comment;
import org.wlgzs.moyue.util.Result;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Auther: dayfly
 * @Date: 2018/4/24 21:30
 * @Description:
 */
public interface CommentServicee {
    //用户页面显示个人评论
    ModelAndView commentsByUserId(int page, HttpSession session, Model model);

    //分页显示用户评论
    ModelAndView commentsByBookId(int page, long bookId, Model model);

    //页面：用户添加评论
    Result addComment(Comment comment, HttpSession session);

    //页面：用户删除个人评论
    Result deleteComment(long id, HttpSession session);

    //用户批量删除评论
    Result deleteComments(long[] nums, HttpSession session);

    //书籍详情显示用户评论
    List<Comment>  commentsByBook(long bookId);
}
