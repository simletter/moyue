package org.wlgzs.moyue.service;

import com.alipay.api.AlipayApiException;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;


public interface UserService {
    MimeMessage createSimpleMail(Session session, String code, String email) throws MessagingException;

    String generateShortUuid();

    boolean updateUser(User user);                    //修改用户资料

    void sendEmail(String email, String code) throws MessagingException;//给邮箱发送验证码

    void payment(HttpServletRequest request, HttpServletResponse response) throws IOException, AlipayApiException;

    void returnUrl(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws AlipayApiException, IOException, ParseException;

    void buyBook(Long bookId, Long user);

    void register(String email, String phone, String password, String userAcount, String thirday);

    //上传用户图片
    ModelAndView uploaadUserImg(MultipartFile file, HttpSession session);

    //修改用户资料
    Result updateUser(User user, String usersBirthday, HttpSession session);

    //显示用户个人资料
    Result userMessage(HttpSession session);
    boolean judgeIdentity (String identity);
    //进入用户后台
    void tobms(Model model,String currentPage);
    //搜索用户
    User searchUser(String userName);
    //删除多个用户
    void delateUsers( Long[] usersId);
    //删除一个用户
    void deleteUser(Long userId);


}

