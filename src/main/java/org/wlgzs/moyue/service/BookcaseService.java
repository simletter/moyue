package org.wlgzs.moyue.service;

import org.springframework.ui.Model;
import org.wlgzs.moyue.entity.User;
import org.wlgzs.moyue.util.Result;

public interface BookcaseService {
    boolean saveBookCase(Long userId, Long bookId);

    void bookcaseView(User user, Model model);

    Result addBookCase(User user, long bookId);

    void bookCaseList(Model model, User user);

    void deleteBookCase(Model model, User user, Long id);
}