package org.wlgzs.moyue.service;

import org.wlgzs.moyue.entity.Book;
import org.wlgzs.moyue.util.Result;

import java.util.List;

public interface BookLinkTypeService {
    //添加类型和书籍多对多关系
    boolean addBookLinkType(long bookId, int[] types);

    //根据类型Id查找书籍
    Result findBooksByTypes(List<Long> types);
}
