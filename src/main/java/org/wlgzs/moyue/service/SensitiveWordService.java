package org.wlgzs.moyue.service;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.wlgzs.moyue.entity.SensitiveWord;
import org.wlgzs.moyue.util.Result;

import java.util.List;

/**
 * @Auther: dayfly
 * @Date: 2018/4/24 19:55
 * @Description:
 */
public interface SensitiveWordService {
    //增加敏感词
    Result addSensitiveWord(String sensitiveWord);

    //删除敏感词
    Result deleteSensitiveWord(long Id);

    //分页显示敏感词
    ModelAndView showAllSensitiveWords(String page,Model model);

    //匹配敏感词
    Result mateWord(String word);
}
