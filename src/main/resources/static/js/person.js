//  进入页面时请求此接口显示用户数据
function message() {
    $.ajax({
        type:"GET",
        url:"/moyue/user/usermessage",
        datatype:"JSON",
        success: function (data) {
            var datas = data.data;
            console.log(datas.userSex);
            $('#userName').val(datas.userName)  ;
            $('#userId').val(datas.userId)  ;
            $('#userSex').val(datas.userSex)  ;
            $('.userBirthday').val(datas.userBirthday) ;
            $('#userRewards').val(datas.userRewards)  ;
            $('#description').val(datas.description) ;
        },
    });
}
message();

// 修改资料后请求此接口，将以上述数据发送后台
function update() {
    $.ajax({
        type:"POST",
        url:"/moyue/user/update",
        data:{
            userName :$('#userName').val(),
            userSex :$('#userSex').val(),
            usersBirthday :$('.userBirthday').val(),
            description :$('#description').val(),
        },
        datatype:"JSON",
        success: function (data) {
            alert('保存成功 ！')
        },
        error: function (msg) {//ajax请求失败后触发的方法
            alert("网络故障");//弹出错误信息
        }
    });
}